//
//  utils.h
//  utils
//
//  Created by Balazs Vidumanszki on 2017. 09. 07..
//  Copyright © 2017. Balazs Vidumanszki. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for utils.
FOUNDATION_EXPORT double utilsVersionNumber;

//! Project version string for utils.
FOUNDATION_EXPORT const unsigned char utilsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <utils/PublicHeader.h>


